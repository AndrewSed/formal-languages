""" Лабораторная №3
Тема "Детерминированный конечный автомат и регулярные выражения"
1. Для заданного регулярного выражения ( /[A-Z][a-z]+/ ) над заданным алфавитом создать ДКА.
2. Реализовать поиск по этому регулярному выражению, используя построенный ДКА. Привести примеры как удачного, так и неудачного поиска.
3. Нарисовать граф переходов ДКА.
"""


if __name__ == '__main__':
    from lab3.lib.regex_builder import *

    pattern_12 = \
        Repeat(
            Choose(
                Concatenate(Literal('a'), Literal('b')),
                Literal('a')
            )
        )

    pattern_22 = \
        Concatenate(
            SymbolClass(Literal('A'), Literal('Z')),
            OneAndRepeat(
                SymbolClass(Literal('a'), Literal('z'))
            )
        )

    # print(pattern.inspect)
    # print(pattern_2.inspect)

    def pattern_1():
        Empty().matches('')
        Empty().matches('a')

    def pattern_2():
        Literal('a').matches('')
        Literal('a').matches('a')

    def pattern_3():
        # pattern = OneAndRepeat(Literal('a'))
        pattern = Repeat(
            Concatenate(
                Literal('a'),
                Choose(Empty(), Literal('b'))
            )
        )
        # pattern = Concatenate(
        #     Literal('a'), Concatenate(Literal('b'), Literal('c'))
        # )
        # pattern = Concatenate(Literal('a'), Literal('b'))
        # pattern = Choose(Literal('a'), Literal('b'))
        print(pattern.inspect)
        pattern.matches('')
        pattern.matches('a')
        pattern.matches('ab')
        pattern.matches('aba')
        pattern.matches('abab')
        pattern.matches('abaab')
        pattern.matches('abba')

    def pattern_4():
        from lab2.nfa_rulebook import NFARulebook, FARule
        from lab2.NFADesign import NFADesign
        from lab3.lib.nfa_simulation import NFASimulation

        rulebook = NFARulebook([
            FARule(1, 'a', 1), FARule(1, 'a', 2), FARule(1, 'ε', 2),
            FARule(2, 'b', 3),
            FARule(3, 'b', 1), FARule(3, 'ε', 2)
        ])
        nfa_design = NFADesign([1], [3], rulebook)
        # nfa_design = Concatenate(
        #     SymbolClass(Literal('A'), Literal('Z')), OneAndRepeat(SymbolClass(Literal('a'), Literal('z')))
        # )
        simulation = NFASimulation(nfa_design)
        start_state = nfa_design.to_nfa().current_states

        print(start_state)
        print(*simulation.discover_states_and_rules([start_state]), sep = '\n')
        print(nfa_design.to_nfa([1, 2]).accepting)
        print(nfa_design.to_nfa({2, 3}).accepting)

        print(simulation.next_state([1, 2], 'a'))
        print(simulation.next_state([1, 2], 'b'))
        print(simulation.next_state([3, 2], 'b'))
        print(simulation.next_state([1, 3, 2], 'b'))
        print(simulation.next_state([1, 3, 2], 'a'))
        print(rulebook.alphabet)
        s1 = simulation.rules_for({1, 2})
        s2 = simulation.rules_for({3, 2})
        print([(rule.start_state, rule.character, rule.accept_state) for rule in s1])
        print([(rule.start_state, rule.character, rule.accept_state) for rule in s2])

    def pattern_5():
        from lab3.lib.nfa_simulation import NFASimulation

        nfa = Concatenate(
            SymbolClass(Literal('A'), Literal('C')), OneAndRepeat(SymbolClass(Literal('a'), Literal('c')))
            # SymbolClass(Literal('A'), Literal('Z')), OneAndRepeat(SymbolClass(Literal('a'), Literal('z')))
        )
        nfa_design = nfa.to_nfa_design
        # nfa_design.matches('Andrew')
        # nfa_design.matches('andrew')
        # nfa_design.matches('AndreW')
        simulation = NFASimulation(nfa_design)
        dfa_design = simulation.to_dfa_design()

        print(nfa.inspect, end = '\n')
        while True:
            print('Слово:', end = ' ')
            string = input()
            print('"' + string + '"', '---', dfa_design.accepts(string), end = '\n\n')
            print(nfa.inspect)

        # print(dfa_design.accepts('AndreW'))
        # print(dfa_design.accepts('andrew'))

    def pattern_6():
        # from lab1.dfa_rulebook import DFARulebook
        from lab2.nfa_rulebook import NFARulebook
        # from lab1.dfa_design import DFADesign
        from lab2.farule import FARule
        from lab2.NFADesign import NFADesign
        from lab3.lib.nfa_simulation import NFASimulation

        # rulebook = DFARulebook([
        #     FARule(1, 'a', 2), FARule(1, 'b', 1),
        #     FARule(2, 'a', 2), FARule(2, 'b', 3),
        #     FARule(3, 'a', 3), FARule(3, 'b', 3)
        # ])

        # print(rulebook.next_state(1, 'a'))
        # print(rulebook.next_state(1, 'b'))
        # print(rulebook.next_state(2, 'b'))
        # from lab1.dfa import DFA

        rulebook = NFARulebook([
            FARule(1, 'a', 1), FARule(1, 'a', 2), FARule(1, 'ε', 2),
            FARule(2, 'b', 3),
            FARule(3, 'b', 1), FARule(3, 'ε', 2)
        ])

        nfa_design = NFADesign([1], [3], rulebook)
        simulation = NFASimulation(nfa_design)

        # dfa_design = DFADesign(1, [3], rulebook)
        # print(dfa_design.accepts('baa'))

        dfa_design = simulation.to_dfa_design()
        print(dfa_design.accepts('aaa'))
        print(dfa_design.accepts('aab'))
        print(dfa_design.accepts('bbbabb'))


    pattern_5()

