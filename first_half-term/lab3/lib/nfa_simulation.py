
class NFASimulation:
    from lab2.NFADesign import NFADesign

    def __init__(self, nfa_design: NFADesign) -> None:
        self.nfa_design = nfa_design
    
    def to_dfa_design(self):
        start_state = self.nfa_design.to_nfa().current_states
        states, rules = self.discover_states_and_rules([start_state])
        accept_states = list(filter(lambda state: self.nfa_design.to_nfa(state).accepting, states))

        from lab1.dfa_design import DFADesign, DFARulebook
        return DFADesign(start_state, accept_states, DFARulebook(rules))

    def discover_states_and_rules(self, states: list):
        from lab2.farule import FARule

        set_states = []
        [set_states.append(state) for state in states if state not in set_states]
        rules = [rule for set_rules in set_states for rule in self.rules_for(set_rules)]
        more_sates = [follows for follows in list(map(FARule.follow, rules))]
        if all(map(lambda state: state in states, more_sates)):
            return [states, rules]
        else:
            super_states = more_sates
            [super_states.append(state) for state in states if state not in super_states]
            return self.discover_states_and_rules(super_states)

    def rules_for(self, state) -> set:
        from lab2.farule import FARule
        state = set(state)
        rules = {FARule(state, character, self.next_state(state, character))
                 for character in self.nfa_design.rulebook.alphabet}
        return rules

    def next_state(self, state, character: str):
        nfa = self.nfa_design.to_nfa(state)
        nfa.read_character(character)
        return nfa.current_states
