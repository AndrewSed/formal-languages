from lab2.NFADesign import NFADesign


class Pattern:
    precedence: int
    to_s: str

    def __init__(self, precedence = None, to_s = None):
        if precedence is not None and to_s is not None:
            self.precedence = precedence
            self.to_s = to_s
        # self.set_precedence(precedence)
        # self.set_to_s(to_s)

    @staticmethod
    def init(precedence, to_s):
        return Pattern(precedence, to_s)

    @property
    def to_nfa_design(self) -> NFADesign:
        return NFADesign(int(), list(), None)

    def set_precedence(self, value):
        self.precedence = value

    def set_to_s(self, value):
        self.to_s = value

    def bracket(self, outer_precedence) -> str:
        string: str
        if self.precedence < outer_precedence:
            string = '(' + self.to_s + ')'
        else:
            string = self.to_s
        return string

    @property
    def inspect(self):
        return '/' + self.to_s + '/'

    def matches(self, string: str):
        mes, string = self.to_nfa_design.accepts(string)
        print("String '" + string + "'", "is " + str(mes))
