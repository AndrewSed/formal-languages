from .pattern import Pattern
from lab2.NFADesign import NFADesign


class Concatenate(Pattern):

    def __init__(self, first, second, precedence = None, to_s = None):
        super().__init__(precedence, to_s)
        self.second = second
        self.first = first

    @property
    def to_nfa_design(self) -> NFADesign:
        """
        Преобразование экземпляра Concatenate в объект NFA

        """
        from lab2.nfa_rulebook import NFARulebook, FARule

        first_nfa_design = self.first.to_nfa_design
        second_nfa_design = self.second.to_nfa_design

        start_state = first_nfa_design.start_state
        accept_states = second_nfa_design.accept_states
        rules = first_nfa_design.rulebook.rules + second_nfa_design.rulebook.rules
        extra_rules = [
            FARule(state, 'ε', second_nfa_design.start_state) for state in first_nfa_design.accept_states
        ]
        rulebook = NFARulebook(rules + extra_rules)

        return NFADesign(start_state, accept_states, rulebook)

    # @property
    # def pattern(self) -> Pattern:
    #     return super().init(self.precedence, self.to_s)

    @property
    def to_s(self) -> str:
        return ''.join([literal.bracket(self.precedence) for literal in [self.first, self.second]])

    @property
    def precedence(self):
        return 1

    # def bracket(self, outer_precedence) -> str:
    #     return self.pattern.bracket(outer_precedence)