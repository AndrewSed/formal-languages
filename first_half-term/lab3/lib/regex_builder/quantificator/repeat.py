from .quantify import *
from lab2.NFADesign import NFADesign


class Repeat(Quantify):

    def __init__(self, pattern: Pattern, precedence = None, to_s = None):
        super().__init__(precedence, to_s)
        self.outer_pattern = pattern

    @property
    def to_nfa_design(self) -> NFADesign:
        """
        Преобразование экземпляра Repeat в объект NFA

        """
        from lab2.nfa_rulebook import NFARulebook, FARule

        pattern_nfa_design = self.outer_pattern.to_nfa_design

        start_state = id(list()) % 10000
        accept_states = pattern_nfa_design.accept_states + [start_state]
        rules = pattern_nfa_design.rulebook.rules
        extra_rules = [
            FARule(accept_state, 'ε', pattern_nfa_design.start_state)
            for accept_state in pattern_nfa_design.accept_states
        ] + [FARule(start_state, 'ε', pattern_nfa_design.start_state)]
        rulebook = NFARulebook(rules + extra_rules)

        return NFADesign(start_state, accept_states, rulebook)

    @property
    def pattern(self) -> Pattern:
        return super().pattern

    @property
    def to_s(self) -> str:
        return self.outer_pattern.bracket(super().precedence) + '*'
