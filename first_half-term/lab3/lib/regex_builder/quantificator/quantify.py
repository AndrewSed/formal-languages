from ..pattern import Pattern


class Quantify(Pattern):
    to_s: str

    def __init__(self, pattern: Pattern, precedence = None, to_s = None):
        super().__init__(precedence, to_s)
        self.outer_pattern = pattern

    @property
    def pattern(self) -> Pattern:
        return super().init(self.precedence, self.to_s)

    @property
    def precedence(self):
        return 2

    def bracket(self, outer_precedence) -> str:
        return self.pattern.bracket(outer_precedence)

    @property
    def inspect(self):
        return self.pattern.inspect
