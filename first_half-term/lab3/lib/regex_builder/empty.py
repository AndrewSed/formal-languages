from .pattern import Pattern
from lab2.NFADesign import NFADesign


class Empty(Pattern):
    start_state: object

    def __init__(self, precedence = None, to_s = None):
        super().__init__(precedence, to_s)

    @property
    def to_nfa_design(self) -> NFADesign:
        """
        Преобразование экземпляра Empty в объект NFA

        """
        from lab2.nfa_rulebook import NFARulebook

        start_state = id(list()) % 10000
        accept_states = [start_state]
        rulebook = NFARulebook([])

        return NFADesign(start_state, accept_states, rulebook)

    @property
    def pattern(self) -> Pattern:
        return super().init(self.precedence, self.to_s)

    @property
    def precedence(self) -> int:
        return 3

    @property
    def to_s(self) -> str:
        return ''

    def bracket(self, outer_precedence) -> str:
        return self.pattern.bracket(outer_precedence)

    @property
    def inspect(self):
        return self.pattern.inspect

    def matches(self, string: str):
        return self.to_nfa_design.accepts(string)
