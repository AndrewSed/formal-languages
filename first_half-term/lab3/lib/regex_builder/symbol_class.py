from .pattern import Pattern
from lab2.NFADesign import NFADesign


class SymbolClass(Pattern):

    def __init__(self, first, second, precedence = None, to_s = None):
        super().__init__(precedence, to_s)
        self.first = first
        self.second = second

    @property
    def to_nfa_design(self) -> NFADesign:
        """
        Преобразование экземпляра SymbolClass в объект NFA

        """
        from lab2.nfa_rulebook import NFARulebook, FARule

        first_nfa_design = self.first.to_nfa_design
        second_nfa_design = self.second.to_nfa_design

        start_state = id(list()) % 10000
        # accept_states = first_nfa_design.accept_states + second_nfa_design.accept_states
        rules = first_nfa_design.rulebook.rules + second_nfa_design.rulebook.rules
        middle_literal = []
        for i in range(ord(rules[0].character) + 1, ord(rules[1].character)):
            from .literal import Literal
            middle_literal += [Literal(chr(i))]

        middle_nfa_design = [literal.to_nfa_design for literal in middle_literal]
        rules += [nfa_design.rulebook.rules[0] for nfa_design in middle_nfa_design]

        accept_state = first_nfa_design.accept_states + second_nfa_design.accept_states
        accept_state += [
            nfa_design.accept_states[0] for nfa_design in middle_nfa_design
        ]

        extra_rules = [
            FARule(start_state, 'ε', nfa_design.start_state) for nfa_design in [first_nfa_design, second_nfa_design] + middle_nfa_design
        ]

        rulebook = NFARulebook(rules + extra_rules)

        return NFADesign(start_state, accept_state, rulebook)

    @property
    def pattern(self) -> Pattern:
        return super().init(self.precedence, self.to_s)

    @property
    def to_s(self) -> str:
        to_set = '-'.join([literal.bracket(self.precedence) for literal in [self.first, self.second]])
        return '[' + to_set + ']'

    @property
    def precedence(self):
        return 3

    def bracket(self, outer_precedence) -> str:
        return self.pattern.bracket(outer_precedence)

    @property
    def inspect(self):
        return self.pattern.inspect
