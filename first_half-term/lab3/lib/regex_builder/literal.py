from .pattern import Pattern
from lab2.NFADesign import NFADesign


class Literal(Pattern):

    def __init__(self, character, precedence = None, to_s = None):
        super().__init__(precedence, to_s)
        self.character = character

    @property
    def to_nfa_design(self) -> NFADesign:
        """
        Преобразование экземпляра Literal в объект NFA

        """
        from lab2.farule import FARule
        from lab2.nfa_rulebook import NFARulebook

        start_state, accept_state = id(list()) % 10000, id(list()) % 10000 + 1
        rule = FARule(start_state, self.character, accept_state)
        rulebook = NFARulebook([rule])

        return NFADesign(start_state, [accept_state], rulebook)

    # @property
    # def pattern(self) -> Pattern:
    #     return super().init(self.precedence, self.to_s)

    @property
    def to_s(self) -> str:
        return self.character

    @property
    def precedence(self):
        return 3

    # def bracket(self, outer_precedence) -> str:
    #     return self.pattern.bracket(outer_precedence)

    # @property
    # def inspect(self):
    #     return self.pattern.inspect

    # def matches(self, string: str):
    #     return self.to_nfa_design.accepts(string)
