from .nfa_rulebook import NFARulebook
from .farule import FARule


class NFA:
    """Класс Недетерминированного конечного автомата"""

    _Q: list = []
    _Sigma: list = []
    _Delta: NFARulebook
    _I: int
    _F: list = []
    """ Примеры данных класса
    
        Q = {1, 2}, 
        Σ = {a, b}, 
        ∆ = {<1, aaa, 1>, <1, ab, 2>, <1, b, 2>, <2, ε, 1>}, 
        I = {2},
        F = {2}
    """

    def __init__(self, start_state = None, accept_states: [] = None, rulebook: NFARulebook = None):
        if start_state is None:
            start_state = 0
        if accept_states is None:
            accept_states = []
        if rulebook is None:
            rulebook = NFARulebook([])

        self._result = []
        self._Delta = rulebook
        self._I = start_state
        self._F = accept_states
        if type(start_state) is int:
            start_state = [start_state]
        self._current_states = start_state

    def init(self, Q: list, Sigma: list, Delta: NFARulebook, I: int, F: list):
        self._Q = Q  # конечное множество состояний
        self._Sigma = Sigma  # алфавит
        self._Delta = Delta  # Набор переходов
        self._I = I  # стартовое состояние
        self._F = F  # множество конечных состояний

    @classmethod
    def add_states(cls, *states: int):
        for s in list(states):
            cls._Q.append(s)

    @classmethod
    def del_states(cls, *states):
        """Удаление перечисленных сосотояний по номерам в списке _Q."""
        for s in states:
            try:
                index = cls._Q.index(s)
                del cls._Q[index]
            except ValueError as e:
                print("{0} Удаление невозможно.".format(e))
        return cls._Q

    @classmethod
    def set_states(cls, *states):
        cls._Q = list(states)

    @classmethod
    def get_states(cls):
        return cls._Q

    @classmethod
    def add_alphabet(cls, *alphabet):
        for alpha in alphabet:
            cls._Sigma.append(alpha)

    @classmethod
    def del_alphabet(cls, *alphabet):
        """Удаление символов из алфавита в ДКА в списке _Sigma."""
        for alph in alphabet:
            try:
                index = cls._Sigma.index(alph)
                del cls._Sigma[index]
            except ValueError as e:
                print('{0}, "{1}" отсутвует в алфавите.'.format(e, alph))
        if not cls._Sigma:
            print("Алфавит - пуст.")
        return cls._Sigma

    @classmethod
    def set_alphabet(cls, *alphabet):
        cls._Sigma = list(alphabet)

    @classmethod
    def get_alphabet(cls):
        return cls._Sigma

    def add_transports(self, *delta: FARule):
        for d in delta:
            if d not in self._Delta.rules:
                self._Delta.add_rule(d)

    def del_transports(self, *delta: int):
        """Удаление переходов по номерам в списке _Delta."""
        for d in delta:
            if 0 < d < len(self._Delta.rules):
                self._Delta.del_rule(d)
            else:
                print("Номер перехода - {0}, вышел за диапазон номеров в списке _Delta.".format(d))
        return self._Delta.rules

    @classmethod
    def get_transports(cls):
        return cls._Delta

    def add_initial(self, *states):
        for value in states:
            self._current_states.append(value)

    def del_initial(self, *states: int):
        for value in states:
            try:
                index = self._current_states.index(value)
                del self._current_states[index]
            except ValueError as e:
                print('{0}'.format(e))
        if not self._I:
            print('Начальные состояния не заданны.')
        return self._I

    @classmethod
    def get_initial(cls):
        return cls._I

    @classmethod
    def add_final(cls, *states: int):
        for value in states:
            cls._F.append(value)

    @classmethod
    def del_final(cls, *states: int):
        for value in states:
            try:
                index = cls._F.index(value)
                del cls._F[index]
            except ValueError as e:
                print('{0}'.format(e))
        if not cls._F:
            print('Завершающие состояния не заданны.')
        return cls._F

    @classmethod
    def get_final(cls):
        return cls._F

    @property
    def accept_states(self):
        return self._F

    @property
    def accepting(self):
        return any([(state in self.current_states) for state in self.accept_states])

    def read_character(self, character):
        self._current_states = self._Delta.next_states(self.current_states, character)

    def read_string(self, string: str, optional_string: bool = False):
        if not optional_string:
            for character in string:
                self.read_character(character)
        else:
            if len(string) == 0:
                string = '\0'
            sub_word: str = ""
            last_states: list = self._current_states

            for index, character in enumerate(string):
                self.read_character(sub_word + character)
                if not self._current_states:
                    sub_word += character
                    if index != len(string) - 1:
                        self._current_states = last_states
                else:
                    sub_word = ""

    @property
    def current_states(self) -> set:
        return self._Delta.follow_free_moves(self._current_states)
