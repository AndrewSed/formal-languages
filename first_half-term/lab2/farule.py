
class FARule:
    def __init__(self, start_state, character, accept_state) -> None:
        super().__init__()
        self.start_state = start_state
        self.character = character
        self.accept_state = accept_state

    def applies_to(self, state, character) -> bool:
        return state == self.start_state and character == self.character

    @property
    def current(self):
        return self.start_state

    def follow(self):
        return self.accept_state
