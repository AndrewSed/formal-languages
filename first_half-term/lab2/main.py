"""
Лабораторная работа №2
Тема "Недетерминированный конечный автомат"
✔ 1. Создать объекты программы для представления недетерминированного конечного автомата
(НКА), его состояний и переходов.
✔ 2. Реализовать процедуры добавления/удаления переходов и состояний, добавления и
удаления начальных и заключительных состояний.
✔ 3. Реализовать процедуру перевода НКА из заданного состояния в другое посредством
одного из допустимых переходов НКА.
✔ 4. Реализовать процедуру/метод работы детерминированного конечного автомата по входной
цепочке символов алфавита НКА. (Решить задачу об окончаниях -ое, -ая, -ие).
"""

from lab2.NFADesign import NFADesign
from lab2.nfa import NFA


alphabet = ["а", "б", "в", "г", "д", "е", "ё", "ж", "з", "и", "й", "к", "л", "м", "н", "о",
            "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы", "ь", "э", "ю", "я", "ε"]

with open('russian.txt', "r") as f:
    words = f.read().splitlines()


def word_verification_ab():
    from lab2.nfa_rulebook import NFARulebook, FARule

    rulebook = NFARulebook([
        FARule(1, 'aaa', 1),
        FARule(1, 'ab', 2),
        FARule(1, 'b', 2),
        FARule(2, 'ε', 1)
    ])

    nfa_design = NFADesign(start_state = 2, accept_states = [2], rulebook = rulebook, optional_string = True)

    nfa_design.add_final(2, 3, 4)
    nfa_design.del_final(4, 3)
    new_rules = [FARule(1, 'asdq', 1)]
    nfa_design.add_rule(*new_rules)
    nfa_design.del_rule(*[nfa_design.get_rulebook().rules.index(new_rules[0])])

    print(nfa_design.accepts('aaab'))
    print(nfa_design.accepts('aaaa'))
    print(nfa_design.accepts('ab'))
    print(nfa_design.accepts('abab'))
    print(nfa_design.accepts('aaaab'))
    print(nfa_design.accepts('aa'))
    print(nfa_design.accepts('aaaabb'))


def word_endings():
    from lab2.nfa_rulebook import NFARulebook, FARule

    rulebook = NFARulebook([
        FARule(1, 'ое', 2),
        FARule(1, 'ая', 2),
        FARule(1, 'ие', 2),
        FARule(2, 'ε', 1),
    ])
    find_word = [word for word in words if 'прошл' in word or 'нынешн' in word or 'год' in word[:3]]    # or 'прошедш' in word
    [rulebook.add_rule(FARule(1, word, 2)) for word in find_word]
    rulebook.add_rules([
        FARule(1, 'о', 3),  # соединительная глассная в сложносочиненных словах
        FARule(3, 'ε', 1),
        FARule(3, 'е', 4),
        FARule(1, 'н', 5),  # суффикс н
        FARule(5, 'ε', 1),
        FARule(5, 'а', 6),
        FARule(6, 'я', 4)
    ])

    nfa_design = NFADesign(start_state = 1, accept_states = [2, 4], rulebook = rulebook, optional_string = True)

    print("\n")
    print(nfa_design.accepts('прошлая'))
    print(nfa_design.accepts('прошлое'))
    print(nfa_design.accepts('прошедшие'))  # False т.к. в выборке отсутсвуют слова с корнем "шедш"
    print(nfa_design.accepts('прошлогодние'))
    print(nfa_design.accepts('нынешняя'))   # False т.к. окончаний яя нет в алфавите


if __name__ == '__main__':
    word_verification_ab()
    word_endings()


