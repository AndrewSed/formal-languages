from .farule import FARule


class NFARulebook:

    def __init__(self, rules: [FARule] = None) -> None:
        if rules is None:
            self.rules = [FARule(None, '', None)]
        else:
            self.rules = rules

    @property
    def alphabet(self) -> list:
        characters = {rule.character for rule in self._rules}
        characters.remove('ε')
        return list(characters)

    def next_states(self, states, character) -> set:
        temp = []
        [temp.extend(self.follow_rules_for(state, character)) for state in states]
        res = set(temp)
        return res

    def follow_free_moves(self, states: set) -> set:
        more_states = self.next_states(states, 'ε')
        if more_states.issubset(states):
            return states
        else:
            return self.follow_free_moves(more_states | set(states))

    def follow_rules_for(self, state, character) -> list:
        from .farule import FARule
        return list(map(FARule.follow, self.rules_for(state, character)))

    def rules_for(self, state, character) -> list:
        rule_list = [rule for rule in self._rules if rule.applies_to(state, character)]
        return rule_list

    @property
    def rules(self):
        return self._rules

    @rules.setter
    def rules(self, value: [FARule]):
        self._rules = value

    def add_rules(self, rules: []):
        self._rules.extend(rules)

    def add_rule(self, rule: FARule):
        self._rules.append(rule)

    def del_rule(self, index: int):
        try:
            del self._rules[index]
        except ValueError as e:
            print(e)
