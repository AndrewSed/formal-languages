
class NFADesign:
    from .nfa import NFA, NFARulebook

    def __init__(self, start_state, accept_states: list, rulebook: NFARulebook, optional_string: bool = False):
        """
        Конструктор класса.

        :param start_state: Список начальных состояний
        :param accept_states: Список заключительных сосотояний
        :param rulebook: Набор правил для переходов состояний
        """

        self.start_state = start_state
        self.accept_states = accept_states
        self.current_states = start_state
        self.rulebook = rulebook
        self.nfa = self.to_nfa()
        self.optional_string = optional_string

    def accepts(self, string) -> (bool, str):
        """
        Анализ слова НКА

        :param string: входное слово
        :return: tuple(bool, str) - результат НКА и входное слово
        """

        self.nfa = self.to_nfa()
        self.nfa.read_string(string, self.optional_string)
        return self.nfa.accepting, string

    def to_nfa(self, current_states = None) -> NFA:
        """
        Создание нового экземпляра класса НКА

        :param current_states: список текущих состояний

        :return: новый объект НКА
        """
        from lab2.nfa import NFA

        if current_states is not None:
            self.current_states = current_states

        if hasattr(self, 'nfa'):
            del self.nfa

        self.nfa = NFA(self.current_states, self.accept_states, self.rulebook)

        return self.nfa

    def add_final(self, *states: int):
        states = set(self.accept_states) | set(list(states))
        self.accept_states = list(states)

    def del_final(self, *states: int) -> list:
        for state in list(states):
            del self.accept_states[self.accept_states.index(state)]
        return self.accept_states

    def add_rule(self, *rules: tuple):
        rules_set = set(self.rulebook.rules) | set(list(rules))
        self.rulebook.rules = list(rules_set)

    def del_rule(self, *rules: int) -> list:
        for index in rules:
            self.rulebook.del_rule(index)
        return self.rulebook.rules

    def set_rulebook(self, *rules: tuple):
        self.rulebook = list(rules)

    def get_rulebook(self):
        return self.rulebook
