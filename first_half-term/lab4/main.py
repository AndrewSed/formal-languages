"""Лабораторная работа №4
Тема "Конечный автомат с магазинной памятью"
1. Построить конечный автомат с магазинной памятью (КАМП) в виде объекта программы.
2. Реализовать процедуру/метод проверки баланса различного вида скобок (или других
ограничителей) с помощью КАМП во входной цепочке символов.
3. Реализовать процедуру/метод построения дерева по входящей цепочке символов
различных типов скобок. Дерево должно отражать отношение вложенности скобок.
4. Нарисовать граф переходов КАМП.
"""

from lab4.stack import Stack
from lab4.npda_rulebook import *
from lab4.npda import NPDA
from lab4.npda_design import NPDADesign

if __name__ == '__main__':

    def test_stack():
        stack = Stack(['$']).push('x').push('y').push('z')
        print(stack.inspect)
        print(stack.top)
        stack = stack.pop()
        print(stack.top)
        stack = stack.pop()
        print(stack.top)

    def test_NPDA():
        def ab() -> NPDA:
            rulebook = NPDARulebook([
                PDARule(1, 'a', 1, '$', ['a', '$']),
                PDARule(1, 'a', 1, 'a', ['a', 'a']),
                PDARule(1, 'a', 1, 'b', ['a', 'b']),
                PDARule(1, 'b', 1, '$', ['b', '$']),
                PDARule(1, 'b', 1, 'a', ['b', 'a']),
                PDARule(1, 'b', 1, 'b', ['b', 'b']),
                PDARule(1, 'ε', 2, '$', ['$']),
                PDARule(1, 'ε', 2, 'a', ['a']),
                PDARule(1, 'ε', 2, 'b', ['b']),
                PDARule(2, 'a', 2, 'a', ['']),
                PDARule(2, 'b', 2, 'b', ['']),
                PDARule(2, 'ε', 3, '$', ['$']),
            ])

            confn = PDAConfiguration(1, Stack(['$']))
            npda = NPDA({confn}, [3], rulebook)

            return npda

        def parentheses() -> NPDADesign:
            rulebook = NPDARulebook([
                PDARule(1, '(', 2, '$', ['b', '$']),
                PDARule(2, '(', 2, 'b', ['b', 'b']),
                PDARule(2, ')', 2, 'b', []),
                PDARule(2, 'ε', 1, '$', ['$']),
            ])

            # confn = PDAConfiguration(1, Stack(['$']))
            # npda = NPDA({confn}, [1], rulebook)
            npda_design = NPDADesign(1, '$', [1], rulebook)

            return npda_design

        def square_brackets() ->NPDADesign:
            rulebook = NPDARulebook([
                PDARule(1, '[', 2, '$', ['b', '$']),
                PDARule(2, '[', 2, 'b', ['b', 'b']),
                PDARule(2, ']', 2, 'b', []),
                PDARule(2, 'ε', 1, '$', ['$']),
            ])

            # confn = PDAConfiguration(1, Stack(['$']))
            # npda = NPDA({confn}, [1], rulebook)
            npda_design = NPDADesign(1, '$', [1], rulebook)

            return npda_design

        npda_design = square_brackets()

        # print(npda_design.accepts('()'))
        # print(npda_design.accepts('((()'))
        # print(npda_design.accepts('())'))
        # print(npda_design.accepts('((((()))))'))
        print(npda_design.accepts('[]]'))
        print(npda_design.accepts('[[[]]'))
        print(npda_design.accepts('[]]'))
        print(npda_design.accepts('[[[[]]]]'))

        # for conf in npda_design.npda.current_confs:
        #     print(conf.get)

    test_NPDA()
