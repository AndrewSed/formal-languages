from .stack import Stack


class PDAConfiguration:
    def __init__(self, state, stack: Stack):
        self.state = state
        self.stack = stack

    @property
    def get(self):
        return {"state": self.state, "stack": self.stack.inspect}


class PDARule:
    def __init__(self, state, character, next_state,
                 pop_ch, push_chs) -> None:
        self.state = state
        self.character = character
        self.next_state = next_state
        self.pop_ch = pop_ch
        self.push_chs = push_chs

    def applies_to(self, conf: PDAConfiguration, ch) -> bool:
        return (self.state == conf.state and
                self.pop_ch == conf.stack.top and
                self.character == ch)

    def follow(self, conf: PDAConfiguration):
        return PDAConfiguration(self.next_state, self.next_stack(conf))

    def next_stack(self, conf: PDAConfiguration):
        popped_stack = conf.stack.pop()
        # revers = self.push_chs.copy()
        # revers.reverse()
        self.push_chs.reverse()

        for ch in self.push_chs:
            temp = popped_stack.push(ch)
            popped_stack = temp

        return popped_stack

