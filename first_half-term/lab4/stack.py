class Stack:
    def __init__(self, contents):
        if contents is None:
            contents = []
        self.contents = contents

    def push(self, character: str):
        content = [character] + self.contents
        new_stack = Stack(content)
        return new_stack

    def pop(self):
        try:
            stack = self.contents[1:]
            return Stack(stack)
        except IndexError:
            return Stack([])

    @property
    def top(self):
        try:
            return self.contents[0]
        except IndexError:
            return ''

    @property
    def inspect(self):
        try:
            pop = ''.join(self.contents[1:])
        except IndexError:
            pop = ''

        top = self.top
        return '#<Stack ({0}){1}>'.format(top, pop)
