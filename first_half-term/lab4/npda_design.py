from lab4.stack import Stack
from lab4.npda import NPDA, PDAConfiguration


class NPDADesign:

    def __init__(self, start_state, bottom_character, accept_states, rulebook):
        self.start_state = start_state
        self.bottom_character = bottom_character
        self.accept_states = accept_states
        self.rulebook = rulebook
        self.npda = self.to_npda()

    def accepts(self, string):
        self.to_npda()
        self.npda.read_string(string)
        return self.npda.accepting, string

    def to_npda(self):
        if hasattr(self, 'npda'):
            del self.npda

        start_stack = Stack([self.bottom_character])
        start_conf = PDAConfiguration(self.start_state, start_stack)
        self.npda = NPDA({start_conf}, self.accept_states, self.rulebook)

        return self.npda
