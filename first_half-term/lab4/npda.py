from .npda_rulebook import NPDARulebook, PDAConfiguration


class NPDA:
    def __init__(self, current_confs: {PDAConfiguration}, accept_states: [int], rulebook: NPDARulebook):
        self._current_confs = current_confs
        self.accept_states = accept_states
        self.rulebook = rulebook

    @property
    def accepting(self) -> bool:
        return any([(conf.state in self.accept_states) for conf in self.current_confs])

    def read_string(self, string: str):
        for character in string:
            self.read_character(character)

    def read_character(self, character):
        self._current_confs = self.rulebook.next_configurations(self.current_confs, character)

    @property
    def current_confs(self) -> set:
        return self.rulebook.follow_free_moves(self._current_confs)
