from .pdarule import PDARule, PDAConfiguration


class NPDARulebook:
    def __init__(self, rules: [PDARule]):
        self.rules = rules

    def next_configurations(self, conf, ch) -> set:
        temp = []
        for c in conf:
            follow = self.follow_rules_for(c, ch)
            temp += follow
        res = set(temp)
        return res

    def follow_rules_for(self, conf, ch) -> list:
        rules = self.rules_for(conf, ch)
        res = [rule.follow(conf) for rule in rules]
        return res

    def rules_for(self, conf, ch) -> list:
        rule_list = [rule for rule in self.rules if rule.applies_to(conf, ch)]
        return rule_list

    def follow_free_moves(self, confs) -> set:
        more_conf = self.next_configurations(confs, 'ε')
        temp_more = list(more_conf)
        for c in confs:
            for i, m in enumerate(temp_more):
                if m.stack.contents == c.stack.contents and m.state == c.state:
                    del temp_more[i]

        if not temp_more:
            return confs
        else:
            return self.follow_free_moves(set(confs) | more_conf)

        # if more_conf.issubset(confs):
        #     return confs
        # else:
        #     return self.follow_free_moves(set(confs) | more_conf)
