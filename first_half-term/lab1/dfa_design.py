
from .dfa_rulebook import DFARulebook
from .dfa import DFA


class DFADesign:
    def __init__(self, start_state, accept_states, rulebook: DFARulebook) -> None:
        self.start_state = start_state
        self.accept_states = accept_states
        self.rulebook = rulebook
    
    def to_dfa(self):
        return DFA(self.start_state, self.accept_states, self.rulebook)
    
    def accepts(self, string):
        dfa = self.to_dfa()
        [dfa.read_character(character) for character in string]
        return dfa.accepting
