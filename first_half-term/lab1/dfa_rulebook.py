from ..lab2.farule import FARule

class DFARulebook:

    def __init__(self, rules: list) -> None:
        self._rules = rules

    def next_state(self, state, character) -> int:
        rule = self.rule_for(state, character)
        if rule:
            return rule[0].follow()
        return int()
    
    def rule_for(self, state, character):
        fun = lambda rule: rule.applies_to(state, character)
        rules = list(filter(fun, self._rules))
        return rules

    @property
    def rules(self):
        return self._rules
