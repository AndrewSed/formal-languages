import sys
# import os
#
sys.path.append('.')
from .dfa_rulebook import DFARulebook


class DFA:
    """Класс Детерминированного конечного автомата"""

    """ Примеры данных класса
        Q = {1, 2, 3}, 
        Σ = {a, b}, 
        ∆ = {<1, a, 2>, <1, b, 3>, <2, a, 3>, <2, b, 1>, <3, a, 3>, <3, b, 3>}, 
        I = {1},
        F = {1, 2}
    """

    def __init__(self, start_state = None, accept_states: [] = None, rulebook: DFARulebook = None):
        if start_state is None:
            start_state = 0
        if accept_states is None:
            accept_states = []
        if rulebook is None:
            rulebook = DFARulebook([])

        self._result = []
        self._Q = []
        self._Sigma = []
        self._Delta = rulebook

        # if type(start_state) is int:
        #     start_state = [start_state]
        self._I = [start_state]
        self._current_state = start_state

        self._F = accept_states

    def init(self, Q: list, Sigma: list, Delta: DFARulebook, I: list, F: list):
        self._Q = Q  # конечное множество состояний
        self._Sigma = Sigma  # алфавит
        self._Delta = Delta  # Набор переходов
        self._I = I  # стартовое состояние
        self._F = F  # множество конечных состояний

    def add_states(self, *states: int):
        for s in list(states):
            self._Q.append(s)

    def del_states(self, *states):
        """Удаление перечисленных сосотояний по номерам в списке _Q."""
        for s in states:
            try:
                index = self._Q.index(s)
                del self._Q[index]
            except ValueError as e:
                print("{0} Удаление невозможно.".format(e))

    def get_states(self):
        return self._Q

    def add_alphabet(self, *alphabet):
        for alph in alphabet:
            self._Sigma.append(alph)

    def del_alphabet(self, *alphabet):
        """Удаление символов из алфавита в ДКА в списке _Sigma."""
        for alph in alphabet:
            try:
                index = self._Sigma.index(alph)
                del self._Sigma[index]
            except ValueError as e:
                print('{0}, "{1}" отсутвует в алфавите.'.format(e, alph))
        if not self._Sigma:
            print("Алфавит - пуст.")

    def get_alphabet(self):
        return self._Sigma

    def add_transports(self, *delta):
        for d in delta:
            order_delta = [value for value in self._Delta.rules if value[0] == d[0]]
            goal = [g[1] for g in order_delta]
            if d[1] in goal:
                print('Правило перехода терм-го символа "{0}" уже существует:\n\tid: {1}, value:{2}'.format(
                    d[1], delta.index(d), d))
                continue
            self._Delta.rules.append(d)

    def del_transports(self, *delta: int):
        """Удаление перечисленных переходов по номерам в списке _Delta."""
        for d in delta:
            if 0 < d < len(self._Delta.rules):
                del self._Delta.rules[d]
            else:
                print("Номер перехода - {0}, вышел за диапазон номеров в списке _Delta.".format(d))

    def get_transports(self):
        return self._Delta

    def add_initial(self, *states: int):
        for value in states:
            self._I.append(value)

    def del_initial(self, *states: int):
        for value in states:
            try:
                index = self._I.index(value)
                del self._I[index]
            except ValueError as e:
                print('{0}'.format(e))
        if not self._I:
            print('Начальные состояния не заданны.')

    def get_initial(self):
        return self._I

    def add_final(self, *states: int):
        for value in states:
            self._F.append(value)

    def del_final(self, *states: int):
        for value in states:
            try:
                index = self._F.index(value)
                del self._F[index]
            except ValueError as e:
                print('{0}'.format(e))
        if not self._F:
            print('Завершающие состояния не заданны.')

    def get_final(self):
        return self._F

    def run_dfa(self, *words):

        if not self._Q or not self._Sigma or not self._Delta or not self._I or not self._F:
            raise ValueError('Некоторые параметры ДКА не заданы или заданы неверно. Запуск недоступен.')

        def word_dfa(_word, _initial):
            for alpha in _word:
                try:
                    self._Sigma.index(alpha)
                    delta_order = [d for d in self._Delta.rules if d[0] == _initial and d[1] is alpha]
                    for delta in delta_order:
                        if len(_word) == 1:
                            if delta[2] in self._F:
                                return True
                            else:
                                return False
                        return word_dfa(_word[1:], delta[2])
                except ValueError as e:
                    print(e)
                    return False

        for word in words:
            for initial in self._I:
                self._result.append(word_dfa(word, initial))

    def result_dfa(self):
        return self._result

    @property
    def accept_states(self):
        return self._F

    @property
    def accepting(self):
        return self.current_state in self.accept_states

    def read_string(self, string: str):
        for character in string:
            self.read_character(character)

    def read_character(self, character):
        self._current_state = self._Delta.next_state(self.current_state, character)

    @property
    def current_state(self):
        return self._current_state
