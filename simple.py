"""Детерминированный конечный автомат, который выбирает из входной строки слова состоящие только из 0 и 1."""
import pandas as pd

#((1+) (0+))*
if __name__ == '__main__':
    Sigma = ['0', '1']
    Q = ['S1', 'S2']
    q0 = 'S1'
    # delta_dft = pd.DataFrame(Q, index = Sigma).T
    # T = [[{Q[its]: Q[(jts + its + 1) % 2]} for jts in range(len(Sigma))] for its in range(len(Q))]
    delta_dft = lambda its: [{Q[its]: Q[(jts + its + 1) % 2]} for jts in range(len(Sigma))]
    T = [delta_dft(its) for its in range(len(Q))]

    string = '1100010011012113020212101010'
    res = []

    def state(str1, q):
        if str1 == '':
            return False
        x = str1[0]
        if x not in Sigma or not all(T[Q.index(q)][int(x)]):
            return " "

        if x == Sigma[0]:
            q1 = T[Q.index(q)][int(x)].get(q)
        elif x == Sigma[1]:
            q1 = T[Q.index(q)][int(x)].get(q)
        else:
            q1 = q0

        _next = state(str1[1:], q1)

        if x in Sigma:
            if _next is False:
                return x
            else:
                rez = x + _next
                return rez

    print(string)
    word, length = '', 0
    while True:
        word = state(string[length:], q0)
        if not word:
            break
        length = length + len(word)
        if word is not " ":
            if len(res) == 0:
                res = word
            else:
                res += word
    print(res)
